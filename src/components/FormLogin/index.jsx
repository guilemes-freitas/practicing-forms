import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Grid } from "@material-ui/core";

const FormLogin = (props) => {
  const { HandleLogin } = props;
  const formSchema = yup.object().shape({
    username: yup.string().required("Informe seu nome de usuário ou email!"),
    password: yup.string().required("Informe sua senha!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmitFunction = (data) => {
    HandleLogin(data);
  };
  return (
    <section className="form">
      <h3>Login</h3>

      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <Grid container justify="center">
          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                {...register("username")}
                required
              ></input>
              <span className="placeholder">Nome de usuário*</span>
              <p>{errors.username?.message}</p>
            </label>
          </Grid>
          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                type="password"
                {...register("password")}
                required
              ></input>
              <span className="placeholder">Senha*</span>
              <p>{errors.password?.message}</p>
            </label>
          </Grid>
          <Grid item xs="2">
            <button type="submit" className="submitButton">
              Entrar
            </button>
          </Grid>
        </Grid>
      </form>
    </section>
  );
};

export default FormLogin;

import { Link } from "react-router-dom";
import "./styles.css";

const Navbar = (props) => {
  const { logged, HandleLogout } = props;
  return (
    <nav>
      <ul className="menu">
        {!logged ? (
          <>
            <li>
              <Link to="/">Login</Link>
            </li>
            <li>
              <Link to="/signup" className="signup">
                Cadastro
              </Link>
            </li>
          </>
        ) : (
          <li>
            <button onClick={() => HandleLogout()}>Deslogar</button>
          </li>
        )}
      </ul>
    </nav>
  );
};

export default Navbar;

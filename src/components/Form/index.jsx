import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Grid } from "@material-ui/core";

const Form = (props) => {
  const { HandleMembers } = props;
  const formSchema = yup.object().shape({
    username: yup
      .string()
      .required("Nome de usuário obrigatório!")
      .max(18, "Coloque no máximo 18 caracteres!"),
    name: yup.string().required("Nome obrigatório!"),
    email: yup.string().email("Email inválido").required("Email obrigatório!"),
    confirmEmail: yup
      .string()
      .required("Confirme seu Email!")
      .oneOf([yup.ref("email"), null], "Os Emails não estão iguais!"),
    covenant: yup.string().required("De um nome para a sua equipe!"),
    password: yup
      .string()
      .required("Senha obrigatória!")
      .min(8, "Senha muito curta - mínimo de 8 caracteres.")
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]/,
        "Coloque pelo menos uma letra maiúscula, minúscula, um número e um caractere especial"
      ),
    confirmPassword: yup
      .string()
      .required("Confirme sua senha!")
      .oneOf([yup.ref("password"), null], "As senhas não estão iguais!"),
    age: yup
      .string()
      .required("Coloque sua idade!")
      .matches(/^([0-9]{1,3})/, "Coloque um valor numérico!")
      .max(3, "Acho que ninguém vive tanto assim.."),
    terms: yup.bool().oneOf([true], "Você deve aceitar os termos!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmitFunction = (data) => {
    HandleMembers(data);
  };

  return (
    <section className="form">
      <h3>Cadastro</h3>

      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <Grid container justify="center" direction="row">
          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                {...register("username")}
                required
              ></input>
              <span className="placeholder">Nome de usuário*</span>
              <p className="error">{errors.username?.message}</p>
            </label>
          </Grid>

          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                {...register("name")}
                required
              ></input>
              <span className="placeholder">Nome completo*</span>
              <p className="error">{errors.name?.message}</p>
            </label>
          </Grid>

          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                {...register("email")}
                required
              ></input>
              <span className="placeholder">Endereço de Email*</span>
              <p className="error">{errors.email?.message}</p>
            </label>
          </Grid>

          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                {...register("confirmEmail")}
                required
              ></input>
              <span className="placeholder">Confirme seu Email*</span>
              <p className="error">{errors.confirmEmail?.message}</p>
            </label>
          </Grid>

          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                {...register("age")}
                required
              ></input>
              <span className="placeholder">Idade*</span>
              <p className="error">{errors.age?.message}</p>
            </label>
          </Grid>

          <Grid item xs="12">
            <label className="label-field">
              <input
                className="input-field"
                {...register("covenant")}
                required
              ></input>
              <span className="placeholder">Nome de sua equipe*</span>
              <p className="error">{errors.covenant?.message}</p>
            </label>
          </Grid>

          <Grid item xs="12">
            <div className="flex">
              <label className="label-field">
                <input
                  className="input-field password"
                  type="password"
                  {...register("password")}
                  required
                ></input>
                <span className="placeholder">Senha*</span>
                <p className="error">{errors.password?.message}</p>
              </label>
              <label className="label-field">
                <input
                  className="input-field password"
                  type="password"
                  {...register("confirmPassword")}
                  required
                ></input>
                <span className="placeholder">Confirmar senha*</span>
                <p className="error">{errors.confirmPassword?.message}</p>
              </label>
            </div>
          </Grid>
          <Grid item xs="12">
            <input type="checkbox" id="terms" {...register("terms")}></input>
            <label for="terms" class="terms">
              Eu li e aceito os termos de uso
            </label>
            <p className="error">{errors.terms?.message}</p>
          </Grid>
          <Grid item xs="2">
            <button type="submit" className="submitButton">
              Cadastrar
            </button>
          </Grid>
        </Grid>
      </form>
    </section>
  );
};

export default Form;

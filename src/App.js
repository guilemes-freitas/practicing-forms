import "./App.css";
import { Switch, Route, useHistory } from "react-router";
import { useState } from "react";
import Login from "./pages/Login";
import SignUp from "./pages/SignUp";
import Home from "./pages/Home";
import Navbar from "./components/Navbar";
import { AnimatePresence } from "framer-motion";

function App() {
  let history = useHistory();
  const [membersArray, setMembersArray] = useState([]);
  const [memberLogged, setMemberLogged] = useState({});
  const [logged, setLogged] = useState(false);

  const HandleMembers = (data) => {
    const id = membersArray.length ? (membersArray.length + 1).toString() : "1";
    const member = {
      id: id,
      name: data.name,
      username: data.username,
      password: data.password,
      email: data.email,
      covenant: data.covenant,
    };
    setMembersArray([...membersArray, member]);
    console.log(membersArray);
    history.push("/");
  };

  const HandleLogin = (user) => {
    setMemberLogged(
      membersArray.find((member) => {
        return (
          member.password === user.password && member.username === user.username
        );
      })
    );
    memberLogged.id && setLogged(true);
    memberLogged.id && history.push(`/${memberLogged.id}`);
  };
  const HandleLogout = () => {
    setMemberLogged({});
    setLogged(false);
    history.push("/");
  };

  return (
    <div className="App">
      <Navbar logged={logged} HandleLogout={HandleLogout}></Navbar>
      <header className="App-header">
        <AnimatePresence>
          <Switch>
            <Route exact path="/">
              <Login HandleLogin={HandleLogin}></Login>
            </Route>
            <Route exact path="/signup">
              <SignUp HandleMembers={HandleMembers}></SignUp>
            </Route>
            <Route exact path="/:id">
              <Home HandleLogout={HandleLogout} member={memberLogged}></Home>
            </Route>
          </Switch>
        </AnimatePresence>
      </header>
    </div>
  );
}

export default App;

import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    .error{
        margin: 0;
        padding: 0;
        color: #f71313;
        font-size: 14px;
    }
    .label-field{
        position: relative;
        font-size: 14px;
        padding-top 20px;
        margin-bottom: 5px;
    }
    .form{
        font-family: "Gothic A1";
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        background: #1d1d1d;
        width: 555px;
        height: 80vh;
        max-height: 700px;
        border-radius: 7px;
        box-shadow: 0 2px 5px 5px rgba(0, 0, 0, 0.2);
    }
    .form h3{    
        font-family: "Archivo Black";
        font-weight: lighter;
    }
    .form .input-field{
        border-radius: 3px;
        border: 2px solid #6c66ee;
        -webkit-appearance: none;
        -ms-appearance: none;
        -moz-appearance: none;
        outline:none;
        appearance: none;
        background-color: transparent;
        padding: 12px;
        font-size: 12px;
        width:250px;
        color: #e3e3e3;
    }
    .form .password{
        width:100px;
    }
    .submitButton {
        width: 100%;
        height: 2rem;
        background: #6c66ee;
        border: none;
        border-radius: 3px;
        font-weight: bold;
        color: #e3e3e3;
    }
    .label-field .placeholder{
        position: absolute;
        left: 12px;
        top: calc(50% + 10px);
        transform: translateY(-50%);
        color: #8a8a8a;
        transition: top 0.3s ease,
        font-size 0.3s ease,
        color 0.3s ease;
        padding: 0 5px;
    }
    .label-field .input-field:valid + .placeholder,
    .label-field .input-field:focus + .placeholder{
        background: #1d1d1d;
        top: 8px;
        font-size: 10px;
        color: #7894ff;
    }
    .flex{
        display: flex;
        justify-content: center;
    }
    .flex .placeholder{
        top: calc(50% + 3px);
        font-size: 12px;
    }
    .flex .label-field .input-field:valid + .placeholder,
    .flex .label-field .input-field:focus + .placeholder{
        top: 21px;
    }
    .flex .label-field:nth-child(2n){
        margin-left: 1rem;
    }
    .terms{
        font-size: 14px;
    }
    .MuiGrid-item {
        margin: 0;
        margin-bottom: 5px;
        box-sizing: border-box;
    }
`;
export default GlobalStyle;

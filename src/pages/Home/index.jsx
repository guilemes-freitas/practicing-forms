import { useParams } from "react-router-dom";
import { motion } from "framer-motion";
import "./styles.css";

const Home = (props) => {
  const params = useParams();
  const { member } = props;
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      {params.id === member.id ? (
        <>
          <h1>Bem vindo {member && member.username}! </h1>
          <div className="infoContainer">
            {member.name && <span>Nome: {member.name}</span>}
            {member.email && <span>Email: {member.email}</span>}
            {member.covenant && <span>Equipe: {member.covenant}</span>}
          </div>
        </>
      ) : (
        <h1>Página não encontrada</h1>
      )}
    </motion.div>
  );
};

export default Home;

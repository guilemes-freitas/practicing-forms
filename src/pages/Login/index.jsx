import FormLogin from "../../components/FormLogin";
import { motion } from "framer-motion";

const Login = (props) => {
  const { HandleLogin } = props;
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <FormLogin HandleLogin={HandleLogin}></FormLogin>
    </motion.div>
  );
};

export default Login;

import Form from "../../components/Form";
import { motion } from "framer-motion";

const SignUp = (props) => {
  const { HandleMembers } = props;
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <Form HandleMembers={HandleMembers}></Form>
    </motion.div>
  );
};

export default SignUp;
